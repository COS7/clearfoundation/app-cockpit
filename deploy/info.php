<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'cockpit';
$app['version'] = '1.0.0';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('cockpit_app_description');
$app['tooltip'] = lang('cockpit_app_tooltip');
$app['powered_by'] = array(
    'vendor' => array(
        'name' => 'Cockpit Project',
        'url' => 'https://cockpit-project.org/'
    ),
    'packages' => array(
        'cockpit' => array(
            'name' => 'Cockpit',
        ),
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('cockpit_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = lang('base_subcategory_management');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-base-core >= 1:1.2.6',
    'app-network-core', 
    'app-storage-core >= 1:1.4.7',
    'cockpiit >= 173.2-1'
);

$app['core_file_manifest'] = array( 
    'cockpit_default.conf' => array ( 'target' => '/etc/clearos/storage.d/cockpit_default.conf' ),
    'cockpit.php'=> array( 'target' => '/var/clearos/base/daemon/cockpit.php' ),
);

$app['core_directory_manifest'] = array(
    '/var/clearos/cockpit' => array(),
    '/var/clearos/cockpit/backup' => array(),
);

$app['delete_dependency'] = array(
    'app-cockpit-core',
    'cockpit'
);
