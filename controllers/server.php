<?php

/**
 * Cockpit daemon controller.
 *
 * @category   apps
 * @package    cockpit
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/cockpit/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

require clearos_app_base('base') . '/controllers/daemon.php';

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * MariaDB daemon controller.
 *
 * @category   apps
 * @package    cockpit
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/cockpit/
 */

class Server extends Daemon
{
    /**
     * Server constructor.
     */

    function __construct()
    {
        parent::__construct('cockpit-ws', 'cockpit');
    }

    /**
     * Status.
     *
     * @return view
     */

  //   function status()
  //   {
  //       header('Cache-Control: no-cache, must-revalidate');
  //       header('Content-type: application/json');
  //
  //       //$this->load->library('samba_common/Nmbd');
  //       $this->load->library('cockpit/Cockpit_ws');
  //
  //       //$nmbd_running = $this->nmbd->get_running_state();
  //       $cockpit_running = $this->cockpit_ws->get_running_state();
  //
  //       //$status['status'] = ($smbd_running && $nmbd_running) ? Daemon_Class::STATUS_RUNNING : Daemon_Class::STATUS_STOPPED;
  //       $status['status'] = ($cockpit_running) ? Daemon_Class::STATUS_RUNNING : Daemon_Class::STATUS_STOPPED;
	// //$status['status'] = "running";
	// //$status['status'] = "stopped";
  //
  //       echo json_encode($status);
  //   }

    /**
     * Full daemon status.
     *
     * @return json daemon status encoded in json
     */

    function full_status()
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->load->library('cockpit/Cockpit');

        $status['status'] = $this->cockpit->get_status();
        $status['is_password_set'] = $this->cockpit->is_root_password_set();

        echo json_encode($status);
    }
}
