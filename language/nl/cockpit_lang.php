<?php

$lang['cockpit_app_name'] = 'Cockpitserver';
$lang['cockpit_app_description'] = 'Cockpit is een webgebaseerd beheerplatform voor Linux-servers. Het richt zich op snelheid en veiligheid.';
$lang['cockpit_app_tooltip'] = 'Het account van Cockpit is hetzelfde als de rootgebruiker van dit systeem.';
$lang['cockpit_cockpit'] = 'Cockpit';
$lang['cockpit_access_denied'] = 'Toegang geweigerd.';
$lang['cockpit_cockpit_storage'] = 'Cockpit opslag';
$lang['cockpit_management_tool'] = 'Cockpit beheertool';
$lang['cockpit_management_tool_help'] = 'Volg de link om toegang te krijgen tot de Cockpit beheertool.';
$lang['cockpit_go_to_management_tool'] = 'Ga naar Cockpit beheertool';
$lang['cockpit_management_tool_not_accessible'] = 'De Cockpit beheertool is niet beschikbaar als de service niet actief is';
